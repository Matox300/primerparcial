using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControlEnemigoE : MonoBehaviour
{
    private GameObject jugador;
    private float rapidez;
    private int hp;
    public GameObject enemigoDividido;
    private List<GameObject> listaEnemigos = new List<GameObject>();
    public Camera camaraEnemigo1;
    public Camera camaraEnemigo2;
    public Camera camaraEnemigo3;
    public Camera camaraEnemigo4;
    public Camera camaraEnemigo5;
    public Camera camaraEnemigo6;

    public GameObject proyectil;
    void Start()
    {
        jugador = GameObject.Find("Jugador");
        rapidez = 4f;
        hp = 100;
        camaraEnemigo1.enabled = false;
        camaraEnemigo2.enabled = false;
        camaraEnemigo3.enabled = false;
        camaraEnemigo4.enabled = false;
        camaraEnemigo5.enabled = false;
        camaraEnemigo6.enabled = false;
    }
    void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }
    public void recibirDaņo()
    {
        hp = hp - 25;

        if (hp <= 0)
        {

            this.desaparecer();


            if (gameObject.CompareTag("enemigoE") == true)
            {
                Ray ray = camaraEnemigo1.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

                GameObject pro;
                pro = Instantiate(proyectil, ray.origin, transform.rotation);

                Rigidbody rb = pro.GetComponent<Rigidbody>();
                rb.AddForce(camaraEnemigo1.transform.forward * 15, ForceMode.Impulse);

                Destroy(pro, 0.75f);
                ///////////////////////////
                Ray ray2 = camaraEnemigo2.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

                GameObject pro2;
                pro2 = Instantiate(proyectil, ray2.origin, transform.rotation);

                Rigidbody rb2 = pro2.GetComponent<Rigidbody>();
                rb2.AddForce(camaraEnemigo2.transform.forward * 15, ForceMode.Impulse);

                Destroy(pro2, 0.75f);
                ///////////////////////////
                Ray ray3 = camaraEnemigo3.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

                GameObject pro3;
                pro3 = Instantiate(proyectil, ray3.origin, transform.rotation);

                Rigidbody rb3 = pro3.GetComponent<Rigidbody>();
                rb3.AddForce(camaraEnemigo3.transform.forward * 15, ForceMode.Impulse);

                Destroy(pro3, 0.75f);

                /////////////////////////////////
                Ray ray4 = camaraEnemigo4.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

                GameObject pro4;
                pro4 = Instantiate(proyectil, ray4.origin, transform.rotation);

                Rigidbody rb4 = pro4.GetComponent<Rigidbody>();
                rb4.AddForce(camaraEnemigo4.transform.forward * 15, ForceMode.Impulse);

                Destroy(pro4, 0.75f);
                /////////////////////////////
                Ray ray5 = camaraEnemigo5.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

                GameObject pro5;
                pro5 = Instantiate(proyectil, ray5.origin, transform.rotation);

                Rigidbody rb5 = pro5.GetComponent<Rigidbody>();
                rb5.AddForce(camaraEnemigo5.transform.forward * 15, ForceMode.Impulse);

                Destroy(pro5, 0.75f);
                ////////////////////////////////////
                Ray ray6 = camaraEnemigo6.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

                GameObject pro6;
                pro6 = Instantiate(proyectil, ray6.origin, transform.rotation);

                Rigidbody rb6 = pro6.GetComponent<Rigidbody>();
                rb6.AddForce(camaraEnemigo6.transform.forward * 15, ForceMode.Impulse);

                Destroy(pro6, 0.75f);

            }
        }

    }

    private void desaparecer()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            recibirDaņo();
        }
    }

}
