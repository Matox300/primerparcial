using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ControlEnemigoA : MonoBehaviour
{
    private GameObject jugador;
    private int rapidez;
    private float magnitudSalto;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        jugador = GameObject.Find("Jugador");
        rapidez = 7;
        magnitudSalto = 5.0f;
        rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
    }

    void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("piso") == true)
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
        }
    }
    

}
