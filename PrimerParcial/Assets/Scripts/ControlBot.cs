using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControlBot : MonoBehaviour
{
    private int hp;
    public GameObject enemigoDividido;
    private List<GameObject> listaEnemigos = new List<GameObject>();   
    public GameObject proyectil;
    void Start()
    {
        hp = 100;        
    }

    public void recibirDanio()
    {
        hp = hp - 25;

        if (hp <= 0)
        {

            this.desaparecer();

            if (gameObject.CompareTag("enemigoC")==true)
            {
                listaEnemigos.Add(Instantiate(enemigoDividido, transform.position, Quaternion.identity));
                listaEnemigos.Add(Instantiate(enemigoDividido, transform.position, Quaternion.identity));
            }
            
                
            
        }

    }

    private void desaparecer()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            recibirDanio();
        }
    }

}
