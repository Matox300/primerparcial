using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEnemigoB : MonoBehaviour
{
    private GameObject jugador;
    private float rapidez;
    private Rigidbody rb;
    private Vector3 escalaEnemigo;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        jugador = GameObject.Find("Jugador");
        rapidez = 5;
        escalaEnemigo = new Vector3(0.25f, 0.25f, 0.25f);
    }

    void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("bala") == true)
        {
            transform.localScale += escalaEnemigo;
            rapidez += 1.65f;
        }
    }
}
