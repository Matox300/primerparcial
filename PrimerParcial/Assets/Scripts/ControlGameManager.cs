using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ControlGameManager : MonoBehaviour
{
    float tiempoRestante;    
    public GameObject jugador;
    public Camera camaraMuerte;
    public TMPro.TMP_Text textoTiempo;
    public TMPro.TMP_Text textoGameOver;
    public TMPro.TMP_Text textoTutorial;
    void Start()
    {
        ComenzarJuego();
    }


    void ComenzarJuego()
    {
        StartCoroutine(ComenzarCronometro(60));
    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 60)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante >= 0)
        {
            textoTiempo.text = ("Tiempo: " + tiempoRestante);
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }

        if(tiempoRestante <= 0)
        {
            jugador.SetActive(false);
            camaraMuerte.enabled = true;
            textoTutorial.text = "";
            textoGameOver.text = "Game Over!" +
                "             " +
                "Presiona R para reiniciar";
        }
    }

}