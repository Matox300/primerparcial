using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEnemigoC : MonoBehaviour
{
    private GameObject jugador;
    public float rapidez = 2;


    void Start()
    {
        jugador = GameObject.Find("Jugador");
    }

    
    void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }
}
