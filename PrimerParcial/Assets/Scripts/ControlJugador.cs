using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    /************ CONTROLES DE MOVIMIENTO****************/
    private float rapidezDesplazamiento = 6.0f;
    private Rigidbody rb;
    private float magnitudSalto = 5;
    private float salto = 0;
    private float saltoMaximo = 2;
    private float movimientoAdelanteAtras;
    private float movimientoCostados;

    /*************CAMARA Y DISPARO DE PROYECTIL*************/
    public Camera camaraPrimeraPersona;
    public GameObject proyectil;

    /************** POWERUP************/
    private Vector3 escalarJugador;
    private int cantPowerup = 0 ;
    /*********** CAMARA DE MUERTE ***************/
    public Camera camaraMuerte;

    /********** TEXTOS *************/
    public TMPro.TMP_Text textoTutorial;
    public TMPro.TMP_Text textoGameOver;
    public TMPro.TMP_Text textoVictoria;
    public TMPro.TMP_Text textoPowerup;
    public GameObject gameManager;
    /************** AGREGAR ENEMIGOS **************/
    private int limiteUno = 1;
    private int limiteDos = 1;
    private int limiteTres = 1;
    private List<GameObject> listaEnemigos = new List<GameObject>();
    public GameObject enemigoA;
    public GameObject enemigoB;
    public GameObject enemigoC;
    public GameObject enemigoD;
    public GameObject enemigoE;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
        escalarJugador = new Vector3(0.25f, 0.25f, 0.25f);
        camaraMuerte.enabled = false;
        textoPowerup.text = "PowerUps Recolectados: " + cantPowerup;
    }
    void Update()
    {
        movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);


        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetKeyDown(KeyCode.Space) && salto < saltoMaximo)
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            salto++;
            Debug.Log(salto);
        }

        if(Input.GetKeyDown("x"))
        {
            textoTutorial.text = "";
        }

        
        

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));

            GameObject pro;
            pro = Instantiate(proyectil, ray.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * 15, ForceMode.Impulse);

            Destroy(pro, 0.75f);


        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("piso") == true)
        {
            salto = 0;
        }

        if(other.gameObject.CompareTag("powerup")==true)
        {
            transform.localScale += escalarJugador;
            rapidezDesplazamiento += 2.0f;
            cantPowerup++;            
            textoPowerup.text = "PowerUps Recolectados: " + cantPowerup;
        }

        if (other.gameObject.CompareTag("enemigo") == true || other.gameObject.CompareTag("enemigoC") == true || other.gameObject.CompareTag("balaEnemigo") == true || other.gameObject.CompareTag("caidaEscenario") == true)
        {
            gameObject.SetActive(false);
            camaraMuerte.enabled = true;
            textoTutorial.text = "";
            textoGameOver.text = "Game Over!" +
                "             " +
                "Presiona R para reiniciar";
            gameManager.SetActive(false);
        }


        if (other.gameObject.CompareTag("victoria") == true)
        {
            gameObject.SetActive(false);
            camaraMuerte.enabled = true;
            textoTutorial.text = "";
            textoVictoria.text = "Has Ganado!!" +
                "             " +
                "Presiona R para reiniciar";
            gameManager.SetActive(false);
        }

        if (other.gameObject.CompareTag("triggerUno") == true && limiteUno == 1)
        {            
            listaEnemigos.Add(Instantiate(enemigoA, new Vector3(11.7f, 0.65f, 61.21f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(enemigoB, new Vector3(0.62f, 0.80f, 61.21f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(enemigoA, new Vector3(-11.7f, 0.65f, 61.21f), Quaternion.identity));
            limiteUno++;
        }
        if (other.gameObject.CompareTag("triggerDos") == true && limiteDos == 1)
        {            
            listaEnemigos.Add(Instantiate(enemigoC, new Vector3(11.7f, 0.65f, 120f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(enemigoB, new Vector3(0.62f, 0.80f, 120f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(enemigoC, new Vector3(-11.7f, 0.65f,120f), Quaternion.identity));
            limiteDos++;
        }
        if (other.gameObject.CompareTag("triggerTres") == true && limiteTres == 1)
        {
            listaEnemigos.Add(Instantiate(enemigoE, new Vector3(11.7f, 0.65f, 176f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(enemigoD, new Vector3(3.62f, 0.80f, 176f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(enemigoD, new Vector3(-3.62f, 0.65f, 176f), Quaternion.identity));
            listaEnemigos.Add(Instantiate(enemigoE, new Vector3(-11.7f, 0.65f, 176f), Quaternion.identity));
            limiteTres++;
        }
    }

}



