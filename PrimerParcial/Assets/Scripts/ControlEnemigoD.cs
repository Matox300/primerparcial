using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEnemigoD : MonoBehaviour
{
    public Camera camaraEnemigo;
    private GameObject jugador;
    private float rapidez;
    public GameObject proyectil;

    void Start()
    {
        camaraEnemigo.enabled = false;
        jugador = GameObject.Find("Jugador");
        rapidez = 4f;
    }

    
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camaraEnemigo.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(proyectil, ray.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraEnemigo.transform.forward * 15, ForceMode.Impulse);

            Destroy(pro, 1f);
        }
            


        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);

    }
}
